# Warehouses

Assume we have a system to store information about warehouses and amounts of goods.
We have the following data with formats:
* List of warehouse positions. Each record for a particular position is unique.
Format: { positionId: Long, warehouse: String, product: String, eventTime: Timestamp }

* List of amounts. Records for a particular position can be repeated. The latest record means the current amount. Format: { positionId: Long, amount: BigDecimal, eventTime: Timestamp }.

Using Apache Spark, implement the following methods using DataFrame/Dataset API:
* Load required data from files, e.g. csv of json.
* Find the current amount for each position, warehouse, product.
* Find max, min, avg amounts for each warehouse and product.

## Resources

Look for data in the test resource's folder.

## Assumptions

There are no null values in the data. However, the results my contain them.