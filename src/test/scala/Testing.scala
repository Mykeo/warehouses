import org.apache.spark.sql.{DataFrame, Dataset}

object Testing {

  def isProperFrame[T](frame: Dataset[T]): Boolean = {
    val notEmpty = !frame.isEmpty

    lazy val intact = frame.count()
    lazy val unique = frame.distinct().count()

    notEmpty && intact == unique
  }

  def sortColumns(frame: DataFrame): DataFrame = {
    val columns = frame.columns.sorted
    frame.select(columns.head, columns.tail: _*)
  }

  /**
   * Suits for comparing frames with unknown order.
   * The frames must be proper for testing.
   */
  def sameFrames(l: DataFrame, r: DataFrame): Boolean = {
    val ls = sortColumns(l)
    val rs = sortColumns(r)
    ls.except(rs).isEmpty && rs.except(ls).isEmpty
  }

}
