
import org.apache.spark.sql.{DataFrame, Dataset}
import org.scalatest.{BeforeAndAfter, GivenWhenThen}
import org.scalatest.flatspec.AnyFlatSpec


class WarehousesTest extends AnyFlatSpec with BeforeAndAfter with GivenWhenThen {

  import Warehouses._
  import Timestamps._
  import Testing._

  import spark.implicits._

  var products: DataFrame = _
  var warehouses: DataFrame = _
  var latest: DataFrame = _
  var measured: DataFrame = _

  before {

    products = Seq(
      Product(1, 6499.99, "2021-02-17 06:56:11".timestamp),
      Product(1, 7999.99, "2021-02-17 07:13:10".timestamp),
      Product(1, 4599.99, "2021-02-17 09:27:25".timestamp),
      Product(2, 2149.99, "2021-02-15 11:50:36".timestamp),
      Product(2, 2399.99, "2021-02-16 06:43:26".timestamp),
      Product(3, 13099.99, "2021-02-16 09:53:21".timestamp),
      Product(3, 0.00, "2021-02-17 11:57:21".timestamp),
      Product(4, 599.99, "2021-02-13 12:49:45".timestamp)
    ).toDF

    warehouses = Seq(
      Warehouse(1, "Boston", "Socks", "2021-02-17 6:56:11".timestamp),
      Warehouse(2, "Boston", "Shorts", "2021-02-15 11:50:36".timestamp),
      Warehouse(3, "Lowell", "Breeches", "2021-02-16 9:53:21".timestamp),
      Warehouse(4, "Lowell", "Hats", "2021-02-13 12:49:45".timestamp),
      Warehouse(5, "Lynn", "Jackets", "2021-02-17 7:14:32".timestamp),
      Warehouse(6, "Springfield", "Shoes", "2021-02-16 8:55:03".timestamp)
    ).toDF

    latest = Seq(
      (1, "Boston", "Socks", Some(4599.99)),
      (2, "Boston", "Shorts", Some(2399.99)),
      (3, "Lowell", "Breeches", Some(0.00)),
      (4, "Lowell", "Hats", Some(599.99)),
      (5, "Lynn", "Jackets", None),
      (6, "Springfield", "Shoes", None)
    ).toDF("positionId", "warehouse", "product", "amount")

    measured = Seq(
      ("Boston", "Socks", Some(4599.99), Some(7999.99), Some(6366.66)),
      ("Boston", "Shorts", Some(2149.99), Some(2399.99), Some(2274.99)),
      ("Lowell", "Breeches", Some(0.0), Some(13099.99), Some(6550.0)),
      ("Lowell", "Hats", Some(599.99), Some(599.99), Some(599.99)),
      ("Lynn", "Jackets", None, None, None),
      ("Springfield", "Shoes", None, None, None)
    ).toDF("warehouse", "product", "minimum amount", "maximum amount", "average amount")
  }

  "Running 'latestAmounts'" should "produce a DataFrame where each latest product is bound to its warehouse" in {

    Given("Products and Warehouses DataFrames")

    When("both DataFrames are passed to 'latestAmounts'")
    val amounts = latestAmounts(warehouses, products)

    Then("should be proper for testing")
    if (!isProperFrame(amounts)) fail()

    And("match expected result")
    assert(sameFrames(latest, amounts))
  }

  "Running 'computeAmounts'" should "produce list of <min>, <max> and <avg> amounts" in {

    Given("Products and Warehouses DataFrames")

    When("both DataFrames are passed to 'computeAmounts'")
    val amounts = computeAmounts(warehouses, products)

    Then("should be proper for testing")
    if (!isProperFrame(amounts)) fail()

    And("match expected result")
    assert(sameFrames(measured, amounts))
  }


}
