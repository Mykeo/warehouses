
import org.apache.spark.sql.{DataFrame, SparkSession}
import org.apache.spark.sql.types.StructType
import org.apache.spark.sql.functions._

object Warehouses extends App {

  val spark: SparkSession = SparkSession.builder()
    .appName("Warehouse")
    .config("spark.sql.legacy.timeParserPolicy", "LEGACY")
    .master("local[*]")
    .getOrCreate()

  spark.sparkContext.setLogLevel("WARN")

  val productsPath = "src/test/resources/products.json"
  val warehousesPath = "src/test/resources/warehouses.json"

  /**
   * Products DataFrame.
   */
  val products: DataFrame = readJson(productsPath, Product.schema).persist()
  /**
   * Warehouse DataFrame.
   */
  val warehouses: DataFrame = readJson(warehousesPath, Warehouse.schema).persist()

  latestAmounts(warehouses, products).show()
  computeAmounts(warehouses, products).show()

  /**
   * Reads a DataFrame from JSON file given path and schema.
   * @param path Where the JSON file is located.
   * @param schema What schema to be used.
   * @return A DataFrame.
   */
  def readJson(path: String, schema: StructType): DataFrame = {
    spark.read
      .option("timestampFormat", "yyyy-dd-MM HH:mm:ss")
      .option("multiLine", "true")
      .schema(schema)
      .json(path)
  }

  /**
   * Matches current amount for each position, warehouse, product.
   * @param warehouses DataFrame of warehouses.
   * @param products DataFrame of products.
   * @return A DataFrame.
   */
  def latestAmounts(warehouses: DataFrame, products: DataFrame): DataFrame = {
    val latest = products.groupBy(col("positionId"))
      .agg(max("eventTime").as("latest"))

    val latestCol = col("eventTime") === col("latest")
    val latestWithAmounts = products.join(latest, latestCol, "left_semi")

    warehouses.join(latestWithAmounts, Seq("positionId"), "left")
      .select("positionId", "warehouse", "product", "amount")
  }

  /**
   * Computes max, min and avg amounts for each warehouse and product.
   * @param warehouses  DataFrame of warehouses.
   * @param products DataFrame of products.
   * @return A DataFrame.
   */
  def computeAmounts(warehouses: DataFrame, products: DataFrame): DataFrame = {
    val combined = warehouses.join(products, Seq("positionId"), "left")

    combined.groupBy("warehouse", "product")
      .agg(
        round(min("amount"), 2).as("minimum amount"),
        round(max("amount"), 2).as("maximum amount"),
        round(mean("amount"), 2).as("average amount"))
  }
}
