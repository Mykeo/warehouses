import java.sql.Timestamp

/**
 * Contains an implicit class to convert string into timestamp.
 */
object Timestamps {

  /**
   * Contains 'timestamp' method that returns timestamp.
   * @param line A line that contains timestamp
   */
  implicit class Timestamps(line: String) {
    def timestamp: Timestamp = Timestamp.valueOf(line)
  }

}
