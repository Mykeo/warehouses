import org.apache.spark.sql.catalyst.ScalaReflection
import org.apache.spark.sql.types.StructType

import java.sql.Timestamp

case class Warehouse(positionId: Int,
                     warehouse: String,
                     product: String,
                     eventTime: Timestamp)

case object Warehouse {
  val schema: StructType = ScalaReflection.schemaFor[Warehouse].dataType.asInstanceOf[StructType]
}
