import org.apache.spark.sql.catalyst.ScalaReflection
import org.apache.spark.sql.types.StructType

import java.sql.Timestamp

case class Product(positionId: Int,
                   amount: Double,
                   eventTime: Timestamp)

case object Product {
  val schema: StructType = ScalaReflection.schemaFor[Product].dataType.asInstanceOf[StructType]
}
